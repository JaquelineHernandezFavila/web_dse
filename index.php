<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Honda Altozano</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="icon" type="image/png" href="images/favicon.ico" />
    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">
    
    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <style type="text/css">
      p {
  animation-duration: 2s;
  animation-name: slidein;
}

@keyframes slidein {
  from {
    margin-left: 100%;
    width: 300%
  }

  to {
    margin-left: 0%;
    width: 110%;
  }
}

    </style>
     <style type="text/css">
    #loadPage{
      display: block;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      z-index: 1000;
      background-color: black;
      color: white;
    }
    #loadPage p{
      display: block;
      width: 100px;
      height: 30px;
      font-size: 30px;
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      margin: auto;
    }
    
  </style>
  </head>
  
  
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ca4f9091de11b6e3b06bcfb/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

  <body>
   
    <a href="index.php"><p><img src="images/logoo.png" width="70px" height="70px" align="left"></p></a>
    <a href="http://www.grupofame.com" target="blank"><p><img src="images/logofame.png" width="70px" height="70px" align="right"></p></a>
     
    <section > 
    <!--  <a href="index.php"><img alt="Inicio" align="left" src="images/logoo.png" width="80px" height="80px"></a>  -->

      <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar" >
      <div class="container">
     
      
 

      
       
        <!--<img src="images/logoo.png" alt="Inicio" />-->
        
     
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          
          <span class="oi oi-menu"></span> Menu
        </button>


      <!--<a id="start"></a>-->
<?php include('menufinal.php'); ?>
        <a id="start"></a>   


               
        
       </div> 
     </nav>  
   </section>
    <?php include('sidebar.php'); ?>
           <a id="start"></a> 
     <!--<div class="container">
    <a href="http://www.grupofame.com/inicio/"><img alt="Inicio" align="right" src="images/logofame.png" width="80px" height="80px"></a>  
     </div>-->
<div class="hero-wrap js-fullheight">
<img src="images/Home1360x400.jpg"  width="100%" height="393px">   
</div>     
 

    <!--<section class="ftco-section justify-content-end ftco-search">
      
          
      <<div class="container">

        <div class="row no-gutters">

          <div class="col-md-14 nav-link-wrap">
            <div class="nav nav-pills justify-content-center text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link active" id="v-pills-1-tab" style="background: #e02929"data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Cotización</a>

              <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" style="background: #e02929" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Prueba de Manejo</a>

              <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" style="background: #43dd36" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">WhatsApp</a>

              <a class="nav-link" id="v-pills-4-tab" data-toggle="pill" style="background: #e02929" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">Servicio</a>
             
              <!-- <a class="nav-link"  role="tab" class="button" href="https://api.whatsapp.com/send?phone=4432732789&text=En%20que%20podemos%20ayudarte%20?" aria-selected="false">Whatsaap</a><a  id="v-pills-4-tab"  href="#v-pills-4" aria-controls="v-pills-4" </a>-->
           <!-- </div>
          </div>
          <div class="col-md-12 tab-wrap">
            <!--<div class="img">
            <img src="images/logofamee.jpg" align="left" width="200px" height="130px">
          </div>-->
            <!--<div class="tab-content p-4 px-5" id="v-pills-tabContent">

              <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-nextgen-tab">
                <form action="validar.php" class="search-destination">
                  <div class="row">
                    <div class="col-md align-items-end">
                      <div class="form-group">
                        <label>Nombre*</label>
                        <div class="form-field">
                          <div class="icon"><span class="icon-user"></span></div>
                          <input id="name" name="nombre" type="text" placeholder="Nombre Completo" class="form-control"
                                maxlength="30" size="30" pattern="([a-zA-ZÁÉÍÓÚñáéíóú]{1,}[\s]*)+" required>
                                 <script type="text/javascript"> var f3 = new LiveValidation('f3'); f3.add(Validate.Presence, {failureMessage: "Haz fallado, escribe tu nombre por favor!!! "}); </script> 

                        </div>
                      </div>
                    </div>
                    <div class="col-md align-items-end">
                      <div class="form-group">
                        <label >Email*</label>
                        <div class="form-field">
                          <div class="icon"><span class="icon-envelope"></span></div>
                          <input type="email" id="email" name="email" type="text" placeholder="Correo Electronico" class="form-control" required>
                                <script type="text/javascript"> var f4 = new LiveValidation('f4'); f4.add(Validate.Presence, {failureMessage: "Ocurrio un error con tu correo intentalo de nuevo!!!"}); </script>
                        </div>
                      </div>
                    </div>
                    <div class="col-md align-items-end">
                      <div class="form-group">
                        <label for="#">Telefono*</label>
                        <div class="form-field">
                          <div class="icon"><span class="icon-phone"></span></div>
                            <input type="text" id="telefono" name="telefono" type="text" placeholder="Numero de Telefono" class="form-control" pattern="[0-9]{10}" required>
                            <script type="text/javascript"> var f4 = new LiveValidation('f4'); f4.add(Validate.Presence, {failureMessage: "Por favor ingresa tu numero de telofono!!!"}); </script>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md align-items-end">
                      <div class="form-group">
                        <label for="#">Modelo*</label>
                        <div class="form-field">                          
                            <select name="modelo" id="modelo" class="form-control" placeholder="Selecciona">
                              <option value="0">Selecciona</option>
                               <option name="m1" id="Type R">Type R</option>
                               <option name="m2" id="BR-v">BR-V</option>
                               <option name="m3" id="CR-V">CR-V</option>
                               <option name="m4" id="Civic Coupe">CIVIC Coupe</option>
                               <option name="m4" id="Civic">CIVIC</option>
                               <option name="m4" id="HR-V">HR-V</option>
                               <option name="m4" id="FIT">FIT</option>
                               <option name="m4" id="CITY">CITY</option>
                               <option name="m4" id="ACCORD">ACCORD</option>
                               <option name="m4" id="ODYSSEY">ODYSSEY</option>
                               <option name="m4" id="PILOT">PILOT</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="col-md align-self-end">
                      <div class="form-group">
                        <div class="form-field">
                          <input type="submit" value="Enviar" class="btn btn-primary" color="#39a8e0" width="10px">
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-performance-tab">
                <form action="manejo.php" class="search-destination">
                  <div class="row">
                    <div class="col-md align-items-end">
                      <div class="form-group">
                      <label for="#">Nombre*</label>
                        <div class="form-field">
                          <div class="icon"><span class="icon-user"></span></div>
                          <input id="fname" name="nombre" type="text" placeholder="Nombre Completo" class="form-control"
                                maxlength="30" size="30" pattern="([a-zA-ZÁÉÍÓÚñáéíóú]{1,}[\s]*)+" required>
                                 <script type="text/javascript"> var f3 = new LiveValidation('f3'); f3.add(Validate.Presence, {failureMessage: "Haz fallado, escribe tu nombre por favor!!! "}); </script> 

                        </div>
                      </div>
                    </div>
                    <div class="col-md align-items-end">
                      <div class="form-group">
                        <label for="#">Email*</label>
                        <div class="form-field">
                          <div class="icon"><span class="icon-envelope"></span></div>
                          <input type="email" id="email" name="email" type="text" placeholder="Correo " class="form-control" required>
                                <script type="text/javascript"> var f4 = new LiveValidation('f4'); f4.add(Validate.Presence, {failureMessage: "Ocurrio un error con tu correo intentalo de nuevo!!!"}); </script>
                        </div>
                      </div>
                    </div>
                    <div class="col-md align-items-end">
                      <div class="form-group">
                        <label for="#">Telefono*</label>
                        <div class="form-field">
                          <div class="icon"><span class="icon-phone"></span></div>
                            <input type="text" id="telefono" name="telefono" type="text" placeholder="Telefono" class="form-control" pattern="[0-9]{10}" required>
                            <script type="text/javascript"> var f4 = new LiveValidation('f4'); f4.add(Validate.Presence, {failureMessage: "Por favor ingresa tu numero de telofono!!!"}); </script>
                        </div>
                      </div>
                    </div>
                   <div class="col-md align-items-end">
                      <div class="form-group">
                        <label for="#">Dia(preferente)</label>
                        <div class="form-field">
                          <div class="select-wrap">
                            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                            <select name="dia" id="dia" class="form-control">
                              <option value="0">Selecciona</option>
                               <option name="m1" id="Type R">LUNES</option>
                               <option name="m2" id="BR-v">MARTES</option>
                               <option name="m3" id="CR-V">MIERCOLES</option>
                               <option name="m4" id="Civic Coupe">JUEVES</option>
                               <option name="m4" id="Civic">VIERNES</option>
                               <option name="m4" id="HR-V">SABADO</option>
                               
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>  
                    <div class="col-md align-items-end">
                      <div class="form-group">
                        <label for="#">Hora(preferente)</label>
                        <div class="form-field">
                          <div class="select-wrap">
                            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                            <select name="hora" id="hora" class="form-control">
                              <option value="0">Selecciona</option>
                               <option name="m1" id="Type R">8-10:00am</option>
                               <option name="m2" id="BR-v">10-12:00pm</option>
                               <option name="m3" id="CR-V">12-2:00pm </option>
                               <option name="m4" id="Civic Coupe">2-4:00pm</option>                          
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>    
                      <div class="col-md align-items-end">
                      <div class="form-group">
                        <label for="#">Modelo*</label>
                        <div class="form-field">
                          <div class="select-wrap">
                            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                            <select name="modelo" id="modelo" class="form-control">
                              <option value="0">Selecciona</option>
                               <option name="m1" id="Type R">Type R</option>
                               <option name="m2" id="BR-v">BR-V</option>
                               <option name="m3" id="CR-V">CR-V</option>
                               <option name="m4" id="Civic Coupe">CIVIC Coupe</option>
                               <option name="m4" id="Civic">CIVIC</option>
                               <option name="m4" id="HR-V">HR-V</option>
                               <option name="m4" id="FIT">FIT</option>
                               <option name="m4" id="CITY">CITY</option>
                               <option name="m4" id="ACCORD">ACCORD</option>
                               <option name="m4" id="ODYSSEY">ODYSSEY</option>
                               <option name="m4" id="PILOT">PILOT</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <div class="col-md align-self-end">
                      <div class="form-group">
                        <div class="form-field">
                          <input type="submit" value="Enviar" class="btn btn-primary" color="#39a8e0" width="10px">
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-effect-tab">
                            
                   <div class="row">
                    <p>Estas apunto de abrir de una ventana de whatsapp web. ¿Qué area deseas contactar?</p>  
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                       <a href="#"><input type="submit" value="servicio" class="btn btn-primary" ></a>
                         <a href="#"><input type="submit" value="PostVenta" class="btn btn-primary" ></a>
                  </div>
                </div>
   <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-effect-tab">
            <div class="row">
                          <p> Horario de Atención Lunes a Viernes 8:00 a 19:00 hrs Sábados 8:00 a 16:00 hrs </p>
                                        

                       
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      
                     <div class="col-md align-items-end">
                      <div class="form-group">
                         <div class="form-group">
                         <div class="form-field">
                       <a href="servicios.php"><input type="submit" value="Servicio" class="btn btn-primary"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                   <div class="col-md align-items-end">
                      <div class="form-group">
                         <div class="form-group">
                         <div class="form-field">
                       <a href="refacciones.php"><input type="submit" value="Refacciones" class=" btn btn-primary"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                
                  <div class="col-md align-items-end">
                      <div class="form-group">
                         <div class="form-group">
                         <div class="form-field">
                       <a href="contact.php"><input type="submit" value="Contacto" class="btn btn-primary"></a>
                        </div>
                      </div>
                    </div>
                  </div>

          </div>
        </div>
      </div>
    </section>-->

    

        <?php include('fixed.php'); ?>
        <a id="start"></a>   
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>